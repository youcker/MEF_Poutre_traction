function [COOR,DX]=accoor(Nb_Particules,Longueur)

DX=Longueur/(Nb_Particules-1);
COOR=zeros(Nb_Particules,1);
for i=1:Nb_Particules
    COOR(i,1)=COOR(i,1)+(i-1)*DX;
end
end
