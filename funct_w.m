%traction
function [WI,DWIDX,R]=funct_w(xi,xj,h)

rij=xi(1,1)-xj(1,1);
R=abs(rij)/h;

if(R<=1)
    WI=(3/2)*(2/3);
            DWIDX=-8*R+12*R^2;
            D2WIDX2=-8+24*R;
         elseif(0.5<R<=1)
            WI=(4/3)-4*R+4*R^2-(4/3)*R^3;
            DWIDX=-4+8*R-4*R^2;
            D2WIDX2=8-8*R;
         else
            WI=0;
            DWIDX=0;
            D2WIDX2=0;
           end
end
