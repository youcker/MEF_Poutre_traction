clear all
clc
Longueur=pi;
Nb_Particules=11;

[COOR,DX]=accoor(Nb_Particules,Longueur);

u=sin(COOR(:,1));
du=cos(COOR(:,1));
% u=ones(Nb_Particules,1);
uh=zeros(Nb_Particules,1);
den=0;
duh=zeros(Nb_Particules,1);
h=1.1*DX;%h=1.5*DX;
% for i=(Nb_Particules-1)/4:3*(Nb_Particules-1)/4
for i=1:Nb_Particules
    xi=COOR(i,1);
    for j=1:Nb_Particules
        xj=COOR(j,1);
        vj=DX;
    [WI,DWIDX,R]=funct_w(xi,xj,h);
    wi(i,j)=WI;
    rw(i,j)=R;
    uh(i)=uh(i)+WI*vj*(u(j));
%  uh(i)=uh(i)+WI*vj*(u(j));
 duh(i)=duh(i)+DWIDX*vj*(u(i)-u(j));
    end
%     uh(i)=uh(i)/den(i);
end
plot(COOR(:,1),u,'r-o');
hold on
plot(COOR(:,1),uh,'g-o');
hold off
